﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounterClass;

namespace BasicTests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void TestIncrement()
        {
            BasicClass moncompteur = new BasicClass();
            moncompteur.setValue(0);
            moncompteur.Increment();
            Assert.AreEqual(1, moncompteur.getValue());

            moncompteur.setValue(10);
            moncompteur.Increment();
            Assert.AreEqual(11, moncompteur.getValue());
        }
        [TestMethod]
        public void TestDecrement()
        {
            BasicClass moncompteur = new BasicClass();
            moncompteur.setValue(0);
            moncompteur.decrement();
            Assert.AreEqual(0,moncompteur.getValue());

            moncompteur.setValue(10);
            moncompteur.decrement();
            Assert.AreEqual(9, moncompteur.getValue());
        }
        [TestMethod]
        public void TestReset()
        {
            BasicClass moncompteur = new BasicClass();
            moncompteur.setValue(100);
            moncompteur.reset();
            Assert.AreEqual(0, moncompteur.getValue());
        }
    }
}
