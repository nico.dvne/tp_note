﻿using System;

namespace BasicCounterClass
{
    public class BasicClass 
    {
        private int value;
        
        public void Increment()
        {
            this.value++;
        }
        public void decrement()
        {
            if (this.value > 0) this.value--;
        }
        public void reset()
        {
            this.value = 0;
        }
        public int getValue()
        {
            return this.value;
        }
        public void setValue(int value)
        {
            this.value = value;
        }

    }
}
