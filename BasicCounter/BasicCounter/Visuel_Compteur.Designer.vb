﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Visuel_Compteur
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Lbl_Total = New System.Windows.Forms.Label()
        Me.Lbl_Score = New System.Windows.Forms.Label()
        Me.Btn_Plus = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Btn_moins = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Lbl_Total
        '
        Me.Lbl_Total.AutoSize = True
        Me.Lbl_Total.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_Total.Location = New System.Drawing.Point(199, 59)
        Me.Lbl_Total.Name = "Lbl_Total"
        Me.Lbl_Total.Size = New System.Drawing.Size(181, 76)
        Me.Lbl_Total.TabIndex = 0
        Me.Lbl_Total.Text = "Total"
        '
        'Lbl_Score
        '
        Me.Lbl_Score.AutoSize = True
        Me.Lbl_Score.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_Score.Location = New System.Drawing.Point(293, 160)
        Me.Lbl_Score.Name = "Lbl_Score"
        Me.Lbl_Score.Size = New System.Drawing.Size(0, 39)
        Me.Lbl_Score.TabIndex = 1
        '
        'Btn_Plus
        '
        Me.Btn_Plus.Location = New System.Drawing.Point(393, 159)
        Me.Btn_Plus.Name = "Btn_Plus"
        Me.Btn_Plus.Size = New System.Drawing.Size(131, 55)
        Me.Btn_Plus.TabIndex = 2
        Me.Btn_Plus.Text = "+"
        Me.Btn_Plus.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(212, 245)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(131, 55)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Reset"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Btn_moins
        '
        Me.Btn_moins.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_moins.Location = New System.Drawing.Point(35, 159)
        Me.Btn_moins.Name = "Btn_moins"
        Me.Btn_moins.Size = New System.Drawing.Size(131, 55)
        Me.Btn_moins.TabIndex = 4
        Me.Btn_moins.Text = "-"
        Me.Btn_moins.UseVisualStyleBackColor = True
        '
        'Visuel_Compteur
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(534, 329)
        Me.Controls.Add(Me.Btn_moins)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Btn_Plus)
        Me.Controls.Add(Me.Lbl_Score)
        Me.Controls.Add(Me.Lbl_Total)
        Me.Name = "Visuel_Compteur"
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Lbl_Total As Label
    Friend WithEvents Lbl_Score As Label
    Friend WithEvents Btn_Plus As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Btn_moins As Button
End Class
