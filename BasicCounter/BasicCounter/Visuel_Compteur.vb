﻿Imports BasicCounterClass

Public Class Visuel_Compteur

    Dim moncompteur As New BasicClass

    Private Sub Btn_Plus_Click(sender As Object, e As EventArgs) Handles Btn_Plus.Click
        moncompteur.Increment()
        Lbl_Score.Text = moncompteur.getValue()
    End Sub

    Private Sub Visuel_Compteur_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        moncompteur.setValue(0)
        Lbl_Score.Text = moncompteur.getValue()
    End Sub

    Private Sub Btn_moins_Click(sender As Object, e As EventArgs) Handles Btn_moins.Click
        moncompteur.decrement()
        Lbl_Score.Text = moncompteur.getValue()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        moncompteur.reset()
        Lbl_Score.Text = moncompteur.getValue()
    End Sub


End Class
